import UIKit
import Foundation
import NVActivityIndicatorView

class LoaderUtility {
    
    let TAG = 98797
    
    public static let shared = LoaderUtility()
    
    private init() {
        
    }
    
    
    /// This will show the activity indicator on provided view
    ///
    /// - Parameter onView: Passed view on which we need to show the indicator
    public func show(onView: UIView) {
        
        let indicator = NVActivityIndicatorView.init(frame: onView.bounds, type: .ballRotateChase, color: UIColor.white, padding: onView.frame.size.width/2-20)
        indicator.backgroundColor = UIColor.init(hue: 0, saturation: 0, brightness: 0, alpha: 0.5)
        indicator.tag = TAG
        onView.addSubview(indicator)

        indicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: onView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: onView, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: indicator, attribute: .height, relatedBy: .equal, toItem: onView, attribute: .height, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: indicator, attribute: .width, relatedBy: .equal, toItem: onView, attribute: .width, multiplier: 1, constant: 0)
            ])
        
        indicator.startAnimating()
    }
    
    /// This will hide the activity indicator from provided view
    ///
    /// - Parameter onView: Passed view from which we need to hide the indicator
    public func hide(fromView: UIView) {
    
        if let indicator = fromView.viewWithTag(TAG) {
            if(indicator is NVActivityIndicatorView) {
                indicator.removeFromSuperview()
            }
        }
        
    }
}
