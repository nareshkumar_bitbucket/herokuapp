import Foundation
import Alamofire

/// This class is responsible for making **POST**, **GET** & other API requests.
class ServiceManager {

    //MARK:- VARIABLES
    /// Shared instance
    public static let shared = ServiceManager()
    
    /// reachability manager
    private var networkReachability: NetworkReachabilityManager?
    
    /// Making the initializer private to avoid other objects of this class to be created
    private init() {
        //Now this Service class can't be intentiated outside of this file
    }
    
    
    /// Function to check the internet availability
    ///
    /// - Returns: Boolean flag, true means connected, false means no connection
    func isReachable()-> Bool {
        return NetworkReachabilityManager.init(host: "www.google.com")?.isReachable ?? false
    }
    
    //MARK:- GET SERVICE
    /// This function is used to make the post network request.
    ///
    /// - Parameters:
    ///   - apiRequestModel: It contains Api path, required headers, required parameters.
    ///   - responseReciever: This is call back delegate for response reciever
    ///   - responseType: This is generic type used to set the response type
    func dataGetTaskWith<T:Codable>(apiRequestModel: ApiRequestModel, responseReciever: BaseResponseDelegate, responseType: T.Type) {
        
        if !self.isReachable() {
            responseReciever.failure(errorModel: ErrorResponseModel(message: Messages.noInternet.rawValue, code: ErrorType.noInternet.rawValue))
            return
        }
        
        //encoding the url string
        guard let encodedString = (ApiConstants.baseUrl.rawValue + apiRequestModel.getApiPath()).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
            
            responseReciever.failure(errorModel: ErrorResponseModel.init(message:  Messages.somethingWrong.rawValue, code: ErrorType.somethingWrong.rawValue))
            return
        }
        
        //convert the url string to URL
        guard let completeUrl = URL.init(string: encodedString) else {
            
            responseReciever.failure(errorModel: ErrorResponseModel.init(message:  Messages.somethingWrong.rawValue, code: ErrorType.somethingWrong.rawValue))
            return
        }
                
        AF.request(completeUrl, method: .get).responseJSON { (response) in
            do {
                if let data = response.data {
                    let json = try JSONDecoder().decode(T.self, from: data)
                    responseReciever.sucess(responseModel: json)
                }
                else {
                    responseReciever.failure(errorModel: ErrorResponseModel.init(message: ErrorType.somethingWrong.rawValue, code: "FAIL"))
                }
            }
            catch {
                responseReciever.failure(errorModel: ErrorResponseModel.init(message:  error.localizedDescription, code: "FAIL"))
            }
        }
    }
}

