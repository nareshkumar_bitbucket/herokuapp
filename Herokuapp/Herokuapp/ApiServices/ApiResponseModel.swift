import Foundation

/// This is the base Error modal
struct ErrorResponseModel: Codable {
    
    var message : String = ""
    var code    : String = ""
}



