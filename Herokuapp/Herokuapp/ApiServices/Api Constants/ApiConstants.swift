import Foundation

/// This structure contains **Base URL**, **Api Paths**, **Parameter Names** & **Status Codes**
enum ApiConstants: String {
    
    /// This is the base url used get
    case baseUrl                 = "https://stark-spire-93433.herokuapp.com"
        
}
