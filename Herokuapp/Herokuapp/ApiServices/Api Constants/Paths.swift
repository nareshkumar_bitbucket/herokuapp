extension ApiConstants {
    
    /// This structure cotains the  **Api Paths**
    enum Path {
    
        case json
        
        ///Custome URLS
        case custom(String)
        
        ///This value will return the case's corresponding string value
        var value: String {
            
            switch self {
                
            case .json:
                return "/json"
                
            case .custom(let url):
                return url
            }
        }
    }
}
