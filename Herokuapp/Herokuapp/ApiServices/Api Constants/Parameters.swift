extension ApiConstants {
    
    /// This structure contains all api parameter keys.
    enum Parameters: String {
        
        /// Parameter representing search text
        case apiParameter    = "apiParameter"
    }
}
