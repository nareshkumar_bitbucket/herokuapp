import Foundation

/// This is the base response delegate
protocol BaseResponseDelegate: class {
    
    func sucess<T: Codable>(responseModel: T)
    func failure(errorModel: ErrorResponseModel)
}
