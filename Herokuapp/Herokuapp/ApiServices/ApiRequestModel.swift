import Foundation

struct ApiRequestModel: Codable  {
    
    /// This varaible will hold the api headers
    private var headers: [String: String] = [:]
    
    /// This varaibale will hold the api parameters dictionary
    private var parameters: [String: String]?
    
    /// This variable will point to the api path information
    private var path: String = ""
    
    /// Initialiser
    init() {
        //self.setDefaultHeaders()
    }
}

//MARK:- Setters
extension ApiRequestModel {
    /// Mutable function to update the request's paramerers
    ///
    /// - Parameters:
    ///   - key: parameter key
    ///   - value: parameter value
    public mutating func setParameter(for key: ApiConstants.Parameters, value: String) {
        
        self.parameters?[key.rawValue] = value
    }
    
    /// This is used to set the default headers which needs in every api
    private mutating func setDefaultHeaders() {
        self.headers["Content-Type"] = "application/json"
    }
    
    
    /// Function to update the api request's path
    ///
    /// - Parameter path: path string
    public mutating func setPath(path: ApiConstants.Path) {
        self.path = path.value
    }
    
    /// Mutable funcation to update the request's headers
    ///
    /// - Parameters:
    ///   - key: header key
    ///   - value: header value
    public mutating func setHeaders(for key: ApiConstants.Parameters, value: String) {
        
        //If value is empty then return
        if value.count == 0 {
            return
        }
        
        self.headers[key.rawValue] = value
    }
}

//MARK:- Getters
extension ApiRequestModel {
    /// Getter: This will return the api headers
    ///
    /// - Returns: header dictionary
    public func getHeaders()-> [String: String] {
        
        return self.headers
    }
    
    
    /// Getter: This will return the Parameters
    ///
    /// - Returns: parameter dictionary
    public func getParameters()-> [String: String]? {
        
        return self.parameters
    }
    
    
    /// Getter: This will return the api path
    ///
    /// - Returns: api path
    public func getApiPath()-> String {
        
        return self.path
    }
}


