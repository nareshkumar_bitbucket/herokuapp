import Foundation

/// Here are the list of error & success messages we will get from api response code
enum Messages: String {
    
    case noInternet     = "It seems you are not connected with Internet"
    
    case somethingWrong = "Something went wrong, Please try after sometime"

}


    
