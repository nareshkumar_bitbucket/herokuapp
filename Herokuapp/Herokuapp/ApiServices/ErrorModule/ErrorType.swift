import Foundation

/// These are the error types.
enum ErrorType: String {
    
    case somethingWrong = "1000"
    
    case noInternet     = "1001"
}
