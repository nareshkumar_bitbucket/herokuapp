
import UIKit

extension UIViewController {
    public func loader(flag: Bool) {
        if(flag) {
            LoaderUtility.shared.show(onView: self.view)
        }
        else {
            LoaderUtility.shared.hide(fromView: self.view)
        }
    }
}
