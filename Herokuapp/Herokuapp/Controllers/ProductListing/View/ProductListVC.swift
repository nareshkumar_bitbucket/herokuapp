import UIKit

class ProductListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-
    private var viewModel: ProductListVM? {
        didSet {
            self.observer()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    private func initialSetup() {
        self.registerTableCellsAndSections()
        self.viewModel = ProductListVM.init()
    }
    
    private func observer() {
        
        self.viewModel?.showIndicator.observe = { [weak self] value in
            self?.loader(flag: value)
        }
        
        self.viewModel?.categories.observe = { [weak self] value in
            self?.tableView.reloadData()
        }
    }
}



//MARK:- TableView
extension ProductListVC: UITableViewDelegate, UITableViewDataSource {
    
    private func registerTableCellsAndSections() {
        
        let sectionViewIdentifier = String(describing: SectionView.self)
        self.tableView.register(UINib.init(nibName: sectionViewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: sectionViewIdentifier)
        
        let cellIdentifier = String(describing: ProductListCell.self)
        self.tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel?.getCategories().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.getProductOfCategory(at: section).count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = String(describing: ProductListCell.self)
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ProductListCell else { return UITableViewCell.init() }
        
        guard let model = self.viewModel?.getProduct(at: indexPath.row, ofCategoryIndex: indexPath.section) else { return cell }
        
        cell.bind(product: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let category = self.viewModel?.getCategory(at: section) else { return nil }
        
        let sectionViewIdentifier = String(describing: SectionView.self)
        
        guard let section = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionViewIdentifier) as? SectionView else { return nil }
        
        section.bind(category: category)
        
        return section
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return SectionView.HEIGHT
    }
}


//MARK:- Sorting
extension ProductListVC  {
    
    @IBAction func sortButtonTapped(sender: UIBarButtonItem) {
        self.showSortingOptionAlert()
    }
}


//MARK:- Alert
extension ProductListVC {
    
    private func showSortingOptionAlert() {
        
        let alert = UIAlertController.init(title: StringConst.sortTitle.rawValue, message: StringConst.sortMessages.rawValue, preferredStyle: .actionSheet)
        let actionMostViewed = UIAlertAction.init(title: String(describing: SortingOption.MostViewed), style: .default) {  [unowned self] (action) in
            self.viewModel?.selectedSorting(option: .MostViewed)
        }
        
        let actionMostShared = UIAlertAction.init(title: String(describing: SortingOption.MostShared), style: .default) {  [unowned self] (action) in
            self.viewModel?.selectedSorting(option: .MostShared)
        }
        
        let actionMostOrdered = UIAlertAction.init(title: String(describing: SortingOption.MostOrdered), style: .default) {  [unowned self] (action) in
            self.viewModel?.selectedSorting(option: .MostOrdered)
        }
        
        let actionCancel = UIAlertAction.init(title: StringConst.cancel.rawValue, style: .cancel) { (action) in
            // do nothing
        }
        
        alert.addAction(actionMostViewed)
        alert.addAction(actionMostShared)
        alert.addAction(actionMostOrdered)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
}
