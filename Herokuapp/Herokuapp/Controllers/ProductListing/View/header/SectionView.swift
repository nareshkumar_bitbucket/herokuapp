import UIKit

class SectionView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    static let HEIGHT: CGFloat = 44.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(category: Category?) {
        
        guard let categoryModal = category else { return }
        
        self.nameLabel.text = categoryModal.name ?? ""
    }
}
