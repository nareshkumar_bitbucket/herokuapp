//
//  VariantCell.swift
//  Herokuapp
//
//  Created by Daffolapmac-83 on 30/07/20.
//  Copyright © 2020 Naresh Kumar. All rights reserved.
//

import UIKit

class VariantCell: UICollectionViewCell {
    
    @IBOutlet weak var sizeLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var colorLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(variant: Variant?) {
        
        guard let variantData = variant else { return }
        
        self.sizeLabel.text = "\(variantData.size ?? 0)"
        self.priceLabel.text = "$\(variantData.price ?? 0)"
        self.colorLabel.text = variantData.color ?? ""
        
    }
}
