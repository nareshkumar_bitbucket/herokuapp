import UIKit
import SwiftSVG

class ProductListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var variantCollectionView: UICollectionView!

    var product: Product?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerCellClass()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(product: Product?) {
        
        guard let countryModal = product else { return }
        
        self.product = product
        
        if let appliedSorting = ProductListVM.APPLIED_SORTING {
            let value = appliedSorting == SortingOption.MostOrdered ? "Mostely Ordered, \(product?.order_count ?? 0)" : (appliedSorting == SortingOption.MostViewed) ? "Mostely Viewed, \(product?.view_count ?? 0)" : "Mostly Shared, \(product?.shares ?? 0)"
            
            self.descLabel.text = "Sorted By: " + value 
        }
        else {
            self.descLabel.text = "Sorted By: None"
        }
            
        self.variantCollectionView.reloadData()
        
        self.nameLabel.text = countryModal.name ?? ""
    }
}

//MARK:- UISetup
extension ProductListCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private func registerCellClass() {
        let cellIdentifier = String(describing: VariantCell.self)
        self.variantCollectionView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return product?.variants.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let nibName = String(describing: VariantCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nibName, for: indexPath) as! VariantCell
        guard let variant = self.product?.variants[indexPath.row] else { return cell }
        cell.bind(variant: variant)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth =  ( collectionView.frame.size.width - 30 ) / 3
        
        return CGSize.init(width: itemWidth, height: 66)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
}





