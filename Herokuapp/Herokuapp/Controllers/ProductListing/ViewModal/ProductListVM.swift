import Foundation

class ProductListVM {
    
    //This object is observed in view controller, when its value get updated the view get will updated
    var categories: Observable<[Category]> = Observable([])
    
    //This is the static variable to hold an information about the current applied sorting
    static var APPLIED_SORTING: SortingOption?
    
    //This is the main root object which have the catagory & ranking products array
    private var categoryRank: CategotyRanking? {
        didSet {
            self.categories.property = categoryRank?.categories
        }
    }
    //This object is observed in view controller, when its value get updated the loading indicator get will updated
    var showIndicator: Observable<Bool> = Observable(false)
    
    //The default initialiser.
    init() {
        //fetch application data
        //Check for internet un-availability. If yes then load data from database
        if !ServiceManager.shared.isReachable() {
            //fetch data from database & set
            DBManager.shared.fetch { [weak self] (data) in
                self?.categories.property = data
            }
        }
        else {
            //Show loader & get data from server
            self.fetchCategotyRanking()
        }
    }
    
    
    /// This will return the category to table
    func getCategories()-> [Category] {
        return self.categories.property ?? []
    }
    
    
    /// This will return the category at index [Section]
    /// - Parameter index: index
    func getCategory(at index: Int)-> Category? {
        
        guard let data = self.categories.property else { return nil }
        
        return index < data.count ? data[index] : nil
    }
    
    
    /// This will return the products of a specific category at index
    /// - Parameter index: category Index
    func getProductOfCategory(at index: Int)-> [Product] {
        
        guard let categoriesArray = self.categories.property else { return [] }
        return categoriesArray.count > index ? categoriesArray[index].products : []
    }
    
    
    /// This will return product at index of category at index
    /// - Parameters:
    ///   - index: Product Index
    ///   - categoryIndex: Category index
    func getProduct(at index: Int, ofCategoryIndex categoryIndex: Int)-> Product? {
        
        guard let categoriesArray = self.categories.property else { return nil }
        
        if (categoryIndex > categoriesArray.count && categoriesArray[categoryIndex].products.count < index) {
            return nil
        }
        else {
            return categoriesArray[categoryIndex].products[index]
        }
    }
    
    /// THis will fetch the category from api
    private func fetchCategotyRanking() {
        var reqModel = ApiRequestModel.init()
        reqModel.setPath(path: .json)
        //self.onProductFetch = onCompletion
        showIndicator.property = true
        ServiceManager.shared.dataGetTaskWith(apiRequestModel: reqModel, responseReciever: self, responseType: CategotyRanking.self)
    }
}

//MARK:- Api Response Handling
extension ProductListVM: BaseResponseDelegate {
    
    
    /// This api success callback
    /// - Parameter responseModel: the response of api
    func sucess<T>(responseModel: T) where T : Decodable, T : Encodable {
        
        showIndicator.property = false
        let response = responseModel as! CategotyRanking
        self.categoryRank = response
        self.setProductAllInformation()
    }
    
    /// This will return the error of api
    /// - Parameter errorModel: error model
    func failure(errorModel: ErrorResponseModel) {
        
        showIndicator.property = false
    }
}

//MARK:- Sorting
extension ProductListVM {
    
    /// This will get call when any sorting option is changes form user view screen
    /// - Parameter option: Selected Sorting Option
    func selectedSorting(option: SortingOption) {
        DispatchQueue.global(qos: .background).async {
            Self.APPLIED_SORTING = option
            guard let tempCategories = self.categories.property else { return }
            let resultedCategories = tempCategories.map { (tempCat) -> Category in
                var newCatagory = tempCat
                let sortedProducts = tempCat.products.sorted { (product1, product2) -> Bool in
                    switch option {
                    case .MostOrdered:
                        return (product1.order_count ?? 0) > (product2.order_count ?? 0)
                    case .MostShared:
                        return (product1.shares ?? 0) > (product2.shares ?? 0)
                    case .MostViewed:
                        return (product1.view_count ?? 0) > (product2.view_count ?? 0)
                    }
                }
                newCatagory.products = sortedProducts
                return newCatagory
            }
            DispatchQueue.main.async {
                self.categories.property = resultedCategories
            }
        }
    }
    
    /// This function will embed the product all information like view count, share count etc
    private func setProductAllInformation() {
        DispatchQueue.global(qos: .background).async {
            guard let tempCategories = self.categories.property else { return }
            let resultedCategories = tempCategories.map { (tempCat) -> Category in
                var newCatagory = tempCat
                let productsWithRankedData = tempCat.products.map { (product) -> Product in
                    let customisedProduct = self.setViewCountOrderAndShares(product: product)
                    return customisedProduct
                }
                newCatagory.products = productsWithRankedData
                return newCatagory
            }
            
            DispatchQueue.main.async {
                DBManager.shared.insert(categories: resultedCategories)
                self.categories.property = resultedCategories
            }
        }
    }
    
    
    /// This func will set the view, order & share count information in an object notation form
    /// - Parameter product: The ranked product
    private func setViewCountOrderAndShares(product: Product) -> Product {
        var customisedProduct = product
        guard let rankList = self.categoryRank?.rankings else { return customisedProduct }
        for rank in rankList {
            rank.products.forEach { (rankedProduct) in
                if (rankedProduct.id ?? 0 == customisedProduct.id ?? -1) {
                    if let viewCount = rankedProduct.view_count {
                        customisedProduct.view_count = viewCount
                    }
                    else if let orderCount = rankedProduct.order_count {
                        customisedProduct.order_count = orderCount
                    }
                    else if let shareCount = rankedProduct.shares {
                        customisedProduct.shares = shareCount
                    }
                }
            }
        }
        return customisedProduct
    }
}


