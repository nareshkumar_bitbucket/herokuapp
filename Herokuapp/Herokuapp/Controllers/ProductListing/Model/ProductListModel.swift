import Foundation

/// The category  ranking root model
struct CategotyRanking  : Codable, Equatable {
    
    var categories      : [Category] = []
    var rankings        : [RankList] = []
    
    static func == (lhs: CategotyRanking, rhs: CategotyRanking) -> Bool {
        return lhs.categories == rhs.categories && lhs.rankings == rhs.rankings
    }
}

/// Category model
struct Category         : Codable, Equatable {
    
    var id              : Int?
    var name            : String?
    var products        : [Product] = []
    var child_categories: [Int] = []
    
    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.products == rhs.products && lhs.child_categories == rhs.child_categories
    }
}

/// Product Model
struct Product          : Codable, Equatable {
    
    var id              : Int?
    var name            : String?
    var date_added      : String?
    var variants        : [Variant] = []
    var view_count      : Int?
    var order_count     : Int?
    var shares          : Int?
    
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.date_added == rhs.date_added && lhs.variants == rhs.variants
    }
}

/// Product variant model
struct Variant          : Codable, Equatable {
    
    var id              : Int?
    var size            : Double?
    var price           : Double?
    var color           : String?
    
    static func == (lhs: Variant, rhs: Variant) -> Bool {
        return lhs.id == rhs.id && lhs.size == rhs.size && lhs.price == rhs.price && lhs.color == rhs.color
    }
}

/// Rank list model
struct RankList         : Codable, Equatable {
    
    var ranking         : String?
    var products        : [RankedProduct] = []
    
    static func == (lhs: RankList, rhs: RankList) -> Bool {
        return lhs.ranking == rhs.ranking && lhs.products == rhs.products
    }
}

/// Ranked Product model
struct RankedProduct    : Codable, Equatable {
    
    var id              : Int?
    var view_count      : Int?
    var order_count     : Int?
    var shares          : Int?
    
    static func == (lhs: RankedProduct, rhs: RankedProduct) -> Bool {
        return lhs.id == rhs.id && lhs.view_count == rhs.view_count && lhs.order_count == rhs.order_count && lhs.shares == rhs.shares
    }
}

