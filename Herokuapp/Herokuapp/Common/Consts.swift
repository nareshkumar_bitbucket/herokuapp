import Foundation

/// Sorting Options
enum SortingOption {
    
    case MostViewed
    
    case MostShared
    
    case MostOrdered
    
}

enum StringConst: String {
    
    case cancel         = "Cancel"
    
    case sortTitle      = "Sort By"
    
    case sortMessages   = "Please one option to sort the data"
    
}



