import Foundation
import CoreData


extension ProductEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProductEntity> {
        return NSFetchRequest<ProductEntity>(entityName: "ProductEntity")
    }

    @NSManaged public var viewCount: Int64
    @NSManaged public var orderCount: Int64
    @NSManaged public var sharesCount: Int64
    @NSManaged public var name: String?
    @NSManaged public var id: Int64
    @NSManaged public var variants: NSSet?

}

// MARK: Generated accessors for variants
extension ProductEntity {

    @objc(addVariantsObject:)
    @NSManaged public func addToVariants(_ value: VariantEntity)

    @objc(removeVariantsObject:)
    @NSManaged public func removeFromVariants(_ value: VariantEntity)

    @objc(addVariants:)
    @NSManaged public func addToVariants(_ values: NSSet)

    @objc(removeVariants:)
    @NSManaged public func removeFromVariants(_ values: NSSet)

}
