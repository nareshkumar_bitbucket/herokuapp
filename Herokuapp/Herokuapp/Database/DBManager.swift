import Foundation
import CoreData

class DBManager {
    
    /// Share instance of database
    static var shared = DBManager()
    
    /// Entity Name
    private let CATEGORY = "CategoryEntity"
    private let PRODUCT = "ProductEntity" 
    private let VARIANT = "VariantEntity"
    
    private let queue = OperationQueue.init()
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Herokuapp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private var viewContext: NSManagedObjectContext?
    
    /// Making it singleton
    private init() {
        
        self.viewContext = persistentContainer.newBackgroundContext()
    }
    
    /// This will add category object to Category Entry
    ///
    /// - Parameter category: array of categorys.
    public func insert(categories: [Category]) {
        
        let operation = Operation.init()
        
        operation.queuePriority = .high
        
        operation.completionBlock = {
            
            categories.forEach { (category) in
                
                //save category object if not present in db
                if !self.isAlreadyExist(categoryId: category.id ?? 0) {
                    
                    let obj = CategoryEntity.init(context: self.viewContext!)
                    obj.id = Int64(category.id ?? 0)
                    obj.name = category.name ?? ""
                    
                    category.products.forEach { (product) in
                        let pro = ProductEntity.init(context: self.viewContext!)
                        pro.id = Int64(product.id ?? 0)
                        pro.name = product.name ?? ""
                        pro.viewCount = Int64(product.view_count ?? 0)
                        pro.sharesCount = Int64(product.shares ?? 0)
                        pro.orderCount = Int64(product.order_count ?? 0)                        
                        product.variants.forEach { (variant) in
                            
                            let variantEnt = VariantEntity.init(context: self.viewContext!)
                            variantEnt.id = Int64(variant.id ?? 0)
                            variantEnt.size = variant.size ?? 0
                            variantEnt.price = variant.price ?? 0
                            variantEnt.color = variant.color ?? ""
                            
                            pro.addToVariants(variantEnt)
                        }
                        obj.addToProducts(pro)
                    }
                }
            }
            
            do {
                try self.viewContext!.save()
            }
            catch {
                print("Exception while saving context")
            }
            
        }
        
        queue.addOperations([operation], waitUntilFinished: true)
    }
    
    /// This will add return the categorys those having the provided string in title.
    ///
    /// - Parameter text type: category title text
    public func fetch(completion: @escaping ([Category]) -> Void)  {
        
        let operation = Operation.init()
        
        operation.queuePriority = .high
        
        operation.completionBlock = {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: self.CATEGORY)
            
            fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "id", ascending: true)]
            
            var categories: [Category] = []
            
            do {
                
                let result = try self.viewContext!.fetch(fetchRequest)
                
                (result as! [NSManagedObject]).forEach { (object) in
                    var category   = Category()
                    
                    category.id = object.value(forKey: "id") as? Int
                    
                    category.name = object.value(forKey: "name") as? String
                    
                    if object.value(forKey: "products") is NSSet {
                        
                        category.products = self.getProducts(managedObjectModel: object)
                    }
                    categories.append(category)
                }
            }
            catch {
                print("Exception while saving context")
            }
            
            
            DispatchQueue.main.async {
                completion(categories)
            }
            
        }
        
        queue.addOperations([operation], waitUntilFinished: true)
    }
    
    /// This will return the product array Model from NSManagedObject
    /// - Parameter managedObjectModel: manages object model
    private func getProducts(managedObjectModel: NSManagedObject?) -> [Product] {
        if let set = managedObjectModel?.value(forKey: "products") as? NSSet {
            var productsArray = [Product]()
            for setItem in set.objectEnumerator() {
                
                let productMo = setItem as? NSManagedObject
                
                var tempProduct = Product()
                tempProduct.id = productMo?.value(forKey: "id") as? Int
                tempProduct.name = productMo?.value(forKey: "name") as? String
                tempProduct.view_count = productMo?.value(forKey: "viewCount") as? Int
                tempProduct.shares = productMo?.value(forKey: "sharesCount") as? Int
                tempProduct.order_count = productMo?.value(forKey: "orderCount") as? Int
                
                if productMo?.value(forKey: "variants") is NSSet {
                    tempProduct.variants = self.getVariants(managedObjectModel: productMo)
                }
                productsArray.append(tempProduct)
            }
            return productsArray
        }
        return []
    }
    
    
    /// This will return the Variant array Model from NSManagedObject
    /// - Parameter managedObjectModel: manages object model
    private func getVariants(managedObjectModel: NSManagedObject?) -> [Variant] {
        if let variantSet = managedObjectModel?.value(forKey: "variants") as? NSSet {
           var variantArray = [Variant]()
           for variantSetItem in variantSet.objectEnumerator() {
               
               let variantMo = variantSetItem as? NSManagedObject
               
               var tempVariant = Variant()
               tempVariant.id = variantMo?.value(forKey: "id") as? Int
               tempVariant.size = variantMo?.value(forKey: "size") as? Double
               tempVariant.color = variantMo?.value(forKey: "color") as? String
               tempVariant.price = variantMo?.value(forKey: "price") as? Double
               
               variantArray.append(tempVariant)
           }
           
           return variantArray
       }

        return []
    }
    
    
    /// This will inform us that the category object is already present in db or not
    ///
    /// - Parameter categoryId: Category Id to compare
    /// - Returns: Boolean flag
    private func isAlreadyExist(categoryId: Int) -> Bool {

        var flag = false

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: CATEGORY)

        let predicate = NSPredicate.init(format: "id == %d", categoryId)

        fetchRequest.predicate = predicate

        do {
            let result = try self.viewContext!.fetch(fetchRequest)

            if result.count != 0 {
                flag = true
            }
        }
        catch {
            print("Exception while saving context")
        }
        return flag

    }
}





