import Foundation
import CoreData


extension VariantEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VariantEntity> {
        return NSFetchRequest<VariantEntity>(entityName: "VariantEntity")
    }

    @NSManaged public var id: Int64
    @NSManaged public var size: Double
    @NSManaged public var price: Double
    @NSManaged public var color: String?

}
